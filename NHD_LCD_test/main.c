/*
 * NHD_LCD_test.c
 *
 * Created: 18/01/2018 03:47:20 p.m.
 * Author : hydro
 */ 


#include "sam.h"
#include "lcd/lcd_NHD.h"
#include "clock/clock_setup.h"
#include "delay/delay_conf.h"


void config()
{
	clock_init();
	REG_WDT_MR = WDT_MR_WDDIS;
	init_LCD_NHD();
	
}


int main(void)
{
    /* Initialize the SAM system */
    SystemInit();
	config();
	

    /* Replace with your application code */
    while (1) 
    {
		char v_array[] = {1,2,3,4,4};
		lcd_clear(1);
		mosCidesi();
		mosVol(v_array);
		Delay_N_ms(400);
    }
}
