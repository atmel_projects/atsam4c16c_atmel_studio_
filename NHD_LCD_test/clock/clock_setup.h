/*
 * clock_setup.h
 *
 * Created: 04/09/2017 09:16:22 a.m.
 *  Author: hydro
 */ 


#ifndef CLOCK_SETUP_H_
#define CLOCK_SETUP_H_

#include "sam.h"

void clock_init();

#endif /* CLOCK_SETUP_H_ */

